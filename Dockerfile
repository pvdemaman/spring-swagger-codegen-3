FROM maven
WORKDIR /app
ADD . /app
RUN mvn clean install
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app/target/spring-swagger-codegen-3-0.0.1-SNAPSHOT.jar"]
